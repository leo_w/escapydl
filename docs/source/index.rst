.. escapydl documentation master file, created by
   sphinx-quickstart on Tue Sep 12 17:15:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eSCAPyDL's documentation!
====================================
.. toctree::
   :maxdepth: 4

   readme


eSCAPyDL documentation
=========================
.. toctree::
   :maxdepth: 4
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
